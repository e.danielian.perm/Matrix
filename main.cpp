#include <iostream>
#include <locale.h>
#include <iomanip>
#include <string>
#include <fstream>
#include <windows.h>
using namespace std;

typedef struct Matrix{
	int**pA;//указатель на первый элемент матрицы
	int rows;//количество строк
	int colomns;//количество столбцов
	Matrix(int n, int m)
	{
		rows = n;
		colomns = m;
	}
	Matrix() {}
};//структура матрицы

void Item1(Matrix*);//операция 1 - вывод матрицы
bool Item15();//операция 15 - выход из программы
void Item2(Matrix*);//операция 2 - сложение
void Item3(Matrix*);//операция 3 - умножение
Matrix* Item4(Matrix*);//операция 4 - транспонирование
Matrix* Item5(Matrix*);//операция 5 - увеличение размеров
void Item6(Matrix*);//операция поиска позиций элементов по значению
void Item7(Matrix*);//операция изменения значения элемента
void Item8(Matrix*);//операция вовзедения в степень
void Item9(Matrix*);//операция 9 - вычисление определителя
void Item10(Matrix*);//операция 10 - вычисление обратной матрицы
void Item11(Matrix*);//операция 11 - вычисление многочлена от матрицы
void Item12(Matrix*);//операция 12 - запись в файл
void Item13(Matrix*);//операция 13 - загрузка из файла
void Item14(Matrix*);//операция 14 - сортировка

Matrix* Multiplication(Matrix*, Matrix*);//умножение 2 матриц
Matrix* Initialization(int, char*[]);//выделение памяти под матрицу и ее инициализация
void PrintMenu(int&);//вывод меню
void DeleteMatrix(Matrix*);//очищение памяти
void CreateMatrix(Matrix*);//динамическое создание
int Determinant(Matrix*);//вычисление определителя
void ClearBuffer();//очистка буфера cin
void Merge(int*, int, int, int);//функция слияния частей массива(используется в алгоритме сортировки)
void MergeSort(int*, int, int);//функцяи сортировки слияниями фон Неймана

int main(int argc, char*argv[])
{
	//вывод диалога
	int oper;
	PrintMenu(oper);
	//определение матрицы
	Matrix*m = new Matrix(1, 1);
	m = Initialization(argc, argv);
	//начинаем работу программы
	do
	{
		//по номеру выбираем "кусок" выолняемого кода
		bool IsNotEmpty = !(m->rows == 0 || m->colomns == 0); //условие простоты матрицы, true если не пустая
		switch (oper)
		{
		case 1: if (IsNotEmpty) Item1(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 2: if (IsNotEmpty) Item2(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 3: if (IsNotEmpty) Item3(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 4: if (IsNotEmpty) m = Item4(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 5: if (IsNotEmpty) m = Item5(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 6: if (IsNotEmpty) Item6(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 7: if (IsNotEmpty) Item7(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 8: if (IsNotEmpty) Item8(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 9: if (IsNotEmpty) Item9(m);
				else cout << "Матрица пустая" << endl;
				break;
		case 10: if (IsNotEmpty) Item10(m);
				 else cout << "Матрица пустая" << endl;
				 break;
		case 11: if (IsNotEmpty) Item11(m);
				 else cout << "Матрица пустая" << endl;
				 break;
		case 12: if (IsNotEmpty) Item12(m);
				 else cout << "Матрица пустая" << endl;
				 break;
		case 13: Item13(m);
				 break;
		case 14: if (IsNotEmpty) Item14(m);
				 else cout << "Матрица пустая" << endl;
				 break;
		case 15: if (Item15())
				 {
				 	DeleteMatrix(m);
				 	return 0;
				 }
				 else break;
		}
		PrintMenu(oper);
	} while (1);//выход выолпнится в switch() case 15
	return 0;
}

void Item1(Matrix* m)
{
	for (int i = 0; i < m->rows; i++)
	{
		for (int j = 0; j < m->colomns; j++)
			cout << setw(3) << m->pA[i][j];
		cout << endl;
	}
}

bool Item15()
{
	cout << "Вы хотите выйти из программы ? (Y/N)";
	string ans;
	while (!(cin>>ans) || !(ans == "y" || ans == "Yes" || ans == "Y" || ans == "yes" || ans == "YES" || 
		     ans == "n" || ans == "No" || ans == "N" || ans == "no" || ans == "NO"))
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cout << "Введен некорректный ответ. Попробуйте еще раз:" << endl;
	}
	if (ans == "y" || ans == "Yes" || ans == "Y" || ans == "yes" || ans == "YES")
	{
		cout << "До свидания!";
		return true;
	}
	if (ans == "n" || ans == "No" || ans == "N" || ans == "no" || ans == "NO")
		return false;
	return false;
}

void PrintMenu(int&p)
{
	setlocale(LC_ALL, "rus");
	cout << "Выберите одну из операций:" << endl
		<< "01. Вывести матрицу" << endl
		<< "02. Сложить матрицу" << endl
		<< "03. Умножить матрицу" << endl
		<< "04. Транспонировать матрицу" << endl
		<< "05. Расширить матрицу" << endl
		<< "06. Найти элементы" << endl
		<< "07. Изменить значение элемента" << endl
		<< "08. Возвести в степень" << endl
		<< "09. Вычислить определитель матрицы" << endl
		<< "10. Вычислить обратную матрицу" << endl
		<< "11. Вычислить многочлен матрицы" << endl
		<< "12. Сохранить в файл" << endl
		<< "13. Загрузить из файла" << endl
		<< "14. Сортировать матрицу" << endl
		<< "15. Выйти из программы" << endl;
	//валидация
	while (!(cin >> p) || p < 1 || p>15)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cout << "Выбрана некорректная операция. Попробуйте еще раз:" << endl;
	}//p - корректная операция т к мы ее проверили
}

Matrix* Initialization(int argc, char*argv[])
{
	Matrix* m = new Matrix(1, 1);//создаем матрицу для работы в пунктах 1-14 (a=NxM)
	CreateMatrix(m);
	if (argc > 1)
	{
		{
			m->rows = 0; m->colomns = 0;
			//определяем N
			int i = 0;
			for (; argv[1][i] >= '0' && argv[1][i] <= '9'; i++) m->rows = m->rows * 10 + argv[1][i] - '0';
			//определяем M
			i++;
			for (; argv[1][i] >= '0' && argv[1][i] <= '9'; i++) m->colomns = m->colomns * 10 + argv[1][i] - '0';
		}
		//создаем мтарицу
		CreateMatrix(m);

		if (argc != 2)//если есть не только размеры
		{//универсально с запятыми, пробелами и ими вместе
			int num = 2, j = 0;//num - текущий номер строки с argv, j - номер текущего элемента матрицы
			for (int i = 0, x = 0; num != argc && j < m->rows*m->colomns; i++) //выполняем пока не кончился массив argv или 
															  //пока не заполнили матрицу полностью
			{
				if (argv[num][i] >= '0' && argv[num][i] <= '9')//если цифра то преобразуем в текущий элемент матрицы
					x = x * 10 + argv[num][i] - '0';
				if (argv[num][i] == ',' || (argv[num][i] == 0 && argv[num][i - 1] != ','))//если встертили запятую или
																			  //конец строки, но в конце строки запятой не было
				{                                                  //то элемнет(число) кончилось и записываем его в мтарицу
					m->pA[j / m->colomns][j%m->colomns] = x;
					x = 0;
					j++;
				}
				if (argv[num][i] == 0)//если конец строки то переходим к следующей строке argv(если кончится, 
				{                     //то num будет равно argc и в цикл мы не зайдем
					num++;
					i = -1;
				}
			}
			//донуляем(если весь массив заполнен, то в цикл не зайдем
			for (; j < m->rows*m->colomns; j++) m->pA[j / m->colomns][j%m->colomns] = 0;
		}
		else//есть только размеры, следовательно зануляем
		{
			for (int i = 0; i < m->rows; i++)
				for (int j = 0; j < m->colomns; j++)
					m->pA[i][j] = 0;
		}
	}
	else
	{
		m->rows = 0; m->colomns = 0;
	}
	return m;
}

void DeleteMatrix(Matrix*m)
{
	for (int i = 0; i < m->rows; i++)
		delete[] m->pA[i];
	delete[] m->pA;
}

void CreateMatrix(Matrix* m)
{
	m->pA = new int*[m->rows];
	for (int i = 0; i < m->rows; i++)
		m->pA[i] = new int[m->colomns];
}

void Item2(Matrix* m1)
{
	//создаем матрицу m2, скоторой будем складывать
	Matrix* m2 = new Matrix(m1->rows, m1->colomns);
	CreateMatrix(m2);
	cout << "Введите матрицу " << m1->rows << "x" << m1->colomns << ":" << endl;
	
	for (int i = 0; i < m2->rows; i++)
		for (int j = 0; j < m2->colomns; j++)
			cin >> m2->pA[i][j];
	cout << "Результат:" << endl;
	for (int i = 0; i < m1->rows; i++)
	{
		for (int j = 0; j < m1->colomns; j++)
			cout << setw(4) << m1->pA[i][j] + m2->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(m2);
}

Matrix* Multiplication(Matrix*m1, Matrix*m2)
{
	Matrix*m_res = new Matrix(m1->rows, m2->colomns);
	//создаем матрицу для умножения и инициализируем ее нулями
	CreateMatrix(m_res);
	for (int i = 0; i < m_res->rows; i++)
		for (int j = 0; j < m_res->colomns; j++)
			m_res->pA[i][j] = 0;
	//выполняем умножение
	for (int i = 0; i < m_res->rows; i++)
		for (int j = 0; j < m_res->colomns; j++)
			for (int h = 0; h < m1->colomns; h++)
				m_res->pA[i][j] += m1->pA[i][h] * m2->pA[h][j];
	return m_res;
}

void Item3(Matrix*m1)
{
	cout << "Введите размеры матрицы: ";
	string s;
	cin >> s;
	Matrix*m2 = new Matrix(0, 0);
	{
		//определяем rows m2
		int i = 0;
		for (; s[i] >= '0' && s[i] <= '9'; i++) m2->rows = m2->rows * 10 + s[i] - '0';
		//определяем colomns m2
		i++;
		for (; s[i] >= '0' && s[i] <= '9'; i++) m2->colomns = m2->colomns * 10 + s[i] - '0';
	}
	if ((m2->colomns) == 0 || (m2->rows) == 0 || (m2->rows) != (m1->colomns))
	{
		cout << "Неверный размер" << endl;
		return;
	}
	CreateMatrix(m2);//выделяем память для m2
	//вводим m2
	cout << "Введите матрицу:" << endl;
	for (int i = 0; i < m2->rows; i++)
		for (int j = 0; j < m2->colomns; j++)
			cin >> m2->pA[i][j];
	Matrix*m_res;
	m_res = Multiplication(m1, m2);
	//выводим m_res
	cout << "Результат:" << endl;
	for (int i = 0; i < m_res->rows; i++)
	{
		for (int j = 0; j < m_res->colomns; j++)
			cout << setw(4) << m_res->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(m2);
	DeleteMatrix(m_res);
}

Matrix* Item4(Matrix*m)
{
	Matrix*m_res = new Matrix(m->colomns, m->rows);//в эту мтарицу запишем тарнспонированную А
	CreateMatrix(m_res);// m2=m1.colomnsXm1.rows
	for (int i = 0; i < m->rows; i++)
		for (int j = 0; j < m->colomns; j++)
			m_res->pA[j][i] = m->pA[i][j];
	
	DeleteMatrix(m);
	return m_res;
}

Matrix* Item5(Matrix* m)
{
#ifdef WIN32
	cout << "Выберите направление:" << endl << "h \33" << endl << "k \30" << endl << "l \32" << endl << "j \31" << endl;
#else
	cout << "Выберите направление:" << endl << "h <" << endl << "k ^" << endl << "l >" << endl << "j v" << endl;
#endif // WIN32
	char direction;//выбранное направление
	cin >> direction;
	if (direction != 'h' && direction != 'k' && direction != 'l' && direction != 'j')
	{
		cout << "Некорректный ввод" << endl;
		return m;
	}
	Matrix*m_res;//создаем расширенную матрицу. при расширении вправо или влево у нас 
	       //увеличивается кол-во столбцов, вверх или вниз - строк
	//смысл расшиерния: создаем расширенную матрицу и записываем в нужном месте
	if (direction == 'h' || direction == 'l')
	{
		m_res = new Matrix(m->rows, m->colomns + 1);
		CreateMatrix(m_res);
		if (direction == 'h')//расширение слева
		{
			for (int i = 0; i < m_res->rows; i++)
				m_res->pA[i][0] = 0;
			for (int i = 0; i < m_res->rows; i++)
				for (int j = 1; j < m_res->colomns; j++)
					m_res->pA[i][j] = m->pA[i][j - 1];
		}
		else//расширение справа
		{
			for (int i = 0; i < m_res->rows; i++)
				m_res->pA[i][m_res->colomns - 1] = 0;
			for (int i = 0; i < m_res->rows; i++)
				for (int j = 0; j < m_res->colomns - 1; j++)
					m_res->pA[i][j] = m->pA[i][j];
		}
		m->colomns;
		DeleteMatrix(m);
	}
	else
	{
		m_res = new Matrix(m->rows + 1, m->colomns);
		CreateMatrix(m_res);
		if (direction == 'k')//расширение сверху
		{
			for (int j = 0; j < m_res->colomns; j++)
				m_res->pA[0][j] = 0;
			for (int i = 1; i < m_res->rows; i++)
				for (int j = 0; j < m_res->colomns; j++)
					m_res->pA[i][j] = m->pA[i - 1][j];
		}
		else//расширение снизу
		{
			for (int j = 0; j < m_res->colomns; j++)
				m_res->pA[m_res->rows - 1][j] = 0;
			for (int i = 0; i < m_res->rows - 1; i++)
				for (int j = 0; j < m_res->colomns; j++)
					m_res->pA[i][j] = m->pA[i][j];
		}
		m->rows;
		DeleteMatrix(m);
	}
	return m_res;
}

void Item6(Matrix*m)
{
	cout << "Введите значение: ";
	int x;//искомое значение
	cin >> x;
	bool WasFound = false;
	for (int i = 0; i < m->rows; i++)
		for (int j = 0; j < m->colomns; j++)
			if (m->pA[i][j] == x)
			{
				if (WasFound)
					cout << ", [" << i << "][" << j << "]";
				else
				{
					cout << "Результат: [" << i << "][" << j << "]";
					WasFound = true;
				}
			}
	if (!WasFound)
		cout << "Элемент не найден" << endl;
	else
		cout << endl;
}

void Item7(Matrix* m)
{
	string s;
	cout << "Введите позицию ([i][j]): ";
	cin >> s;
	//определяем позицию
	int i = 0, j = 0, k;
	for (k = 1; s[k] != ']'; k++)
		i = i * 10 + s[k] - '0';
	for (k += 2; s[k] != ']'; k++)
		j = j * 10 + s[k] - '0';
	if (i >= m->rows || j >= m->colomns)//Некорректная позиция
	{
		cout << "Некорректная позиция" << endl;
		return;
	}
	cout << "Введите новое значение: ";
	int x;//новое значение
	cin >> x;
	m->pA[i][j] = x;
}

void Item8(Matrix* m)
{
	cout << "Укажите степень: ";
	double d_power;
	if (!(cin >> d_power))
	{
		cout << "Некорректный ввод" << endl;
		ClearBuffer();
		return;
	}
	if (d_power < 0)
	{
		cout << "Степень должна быть неотрицательной" << endl;
		return;
	}
	int power = (int)d_power;
	if (fabs(d_power - power) > 1e-5)
	{
		cout << "Степень должна быть целым числом" << endl;
		return;
	}
	if (m->rows != m->colomns)
	{
		cout << "Матрица не квадратная" << endl;
		return;
	}
	//если дошли до этого места, то матрциа и степень удовлетворяют опередлению возведения в степень матрицы
	Matrix* powered_m = new Matrix(m->rows, m->colomns);//Данная матрица в степени. Она может быть по определению только квадратной
	CreateMatrix(powered_m);
	//заполняем итоговую матрицу как единичную
	for (int i = 0; i < powered_m->rows; i++)
		for (int j = 0; j < powered_m->colomns; j++)
			powered_m->pA[i][j] = 0;
	for (int i = 0; i < powered_m->rows; i++) 
		powered_m->pA[i][i] = 1;
	//выполняем вовзедение в степень
	for (int i = 1; i <= power; i++)
		powered_m = Multiplication(powered_m, m);
	cout << "Результат:" << endl;
	for (int i = 0; i < powered_m->rows; i++)
	{
		for (int j = 0; j < powered_m->colomns; j++)
			cout << setw(6) << powered_m->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(powered_m);
}

int Determinant(Matrix*m)
{
	if (m->rows == 1)//определитель матрицы, состоящей из 1 числа равен этому числу
		return m->pA[0][0];
	int s = 0;
	for (int i = 0; i < m->rows; i++)//раскладываем по 0 строке
	{
		Matrix*cofactor = new Matrix(m->rows - 1, m->colomns - 1);//минор, полученный вычеркиванием 0 строки и i-того столбца
		CreateMatrix(cofactor);
		int k = 0;
		for (int _i = 0; _i < m->rows; _i++)
			for (int _j = 0; _j < m->colomns; _j++)
				if (_i != 0 && _j != i)
					cofactor->pA[k / cofactor->colomns][k++ % cofactor->colomns] = m->pA[_i][_j];
		s = s + ((i % 2) ? (-1) : 1)*m->pA[0][i] * Determinant(cofactor);
		DeleteMatrix(cofactor);
	}
	return s;
}

void Item9(Matrix* m)
{
	if (m->rows != m->colomns)
	{
		cout << "Результат: xx" << endl;
		return;
	}
	cout << "Результат: " << Determinant(m) << endl;
}

void Item10(Matrix* m)
{
	if (m->rows != m->colomns)
	{
		cout << "Матрица не квадратная" << endl;
		return;
	}
	int det = Determinant(m);
	if (!det)
	{
		cout << "Данная матрица вырожденна" << endl;
		return;
	}
	//найдем обратную матрицу методом присоединенной матрицы
	Matrix*inverse_m = new Matrix(m->rows, m->colomns);
	CreateMatrix(inverse_m);
	for (int i = 0; i < m->rows; i++)
		for (int j = 0; j < m->colomns; j++)
		{
			Matrix*cofactor = new Matrix(m->rows - 1, m->colomns - 1);
			CreateMatrix(cofactor);
			int k = 0;
			for (int _i = 0; _i < m->rows; _i++)
				for (int _j = 0; _j < m->colomns; _j++)
					if (_i != i && _j != j)
						cofactor->pA[k / cofactor->colomns][k++ % cofactor->colomns] = m->pA[_i][_j];
			inverse_m->pA[j][i] = (((i + j) % 2) ? -1 : 1)*Determinant(cofactor);
			   //inverse_a[j][i] т.к. это транспонирвоанная матрица алгебраических дополнений
			DeleteMatrix(cofactor);
		}
	cout << "Результат:" << endl;
	double inverse_det = 1. / det;
	for (int i = 0; i < inverse_m->rows; i++)
	{
		for (int j = 0; j < inverse_m->colomns; j++)
			cout << setw(6) <<setprecision(2)<< inverse_det*inverse_m->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(inverse_m);
}

void Item11(Matrix* m)
{
	if (m->rows != m->colomns)
	{
		cout << "Многочлен вычисляется только от квадратной матрицы!" << endl;
		return;
	}
	Matrix*result = new Matrix(m->rows, m->colomns);
	CreateMatrix(result);//результирующая матрица
	for (int i = 0; i < result->rows; i++)
		for (int j = 0; j < result->colomns; j++)
			result->pA[i][j] = 0;
	cout << "При запросе: \"Знак: \" введите 1, если текущий одночлен положительный, -1 в противном случае." << endl
		<< "При запросе \"Степень: \" введите степень одночлена. -1 степень будет означать конец ввода." << endl;
	string s = "";//в этой строке сформиурется искомый многочлен
	for (int i = 1; true; i++)//выход осуществляется при вводе -1 в поле степень
	{
		cout << "Ввод " << i << " однoчлена" << endl<<"Знак: ";
		int sign;
		cin >> sign;
		cout << "Степень: ";
		int power;
		cin >> power;
		if (power == -1)
			break;
		Matrix* powered_m = new Matrix(m->rows, m->colomns);
		CreateMatrix(powered_m);//матрица для степени
		//заполняем как единичную
		for (int i = 0; i < powered_m->rows; i++)
			for (int j = 0; j < powered_m->colomns; j++)
				if (i == j)
					powered_m->pA[i][j] = 1;
				else
					powered_m->pA[i][j] = 0;
		for (int i = 1; i <= power; i++)
			powered_m = Multiplication(powered_m, m);
		for (int i = 0; i < result->rows; i++)
			for (int j = 0; j < result->colomns; j++)
				result->pA[i][j] += sign*powered_m->pA[i][j];
		DeleteMatrix(powered_m);
	}
	cout << "Результат:" << endl;
	for (int i = 0; i < result->rows; i++)
	{
		for (int j = 0; j < result->colomns; j++)
			cout << setw(7) << result->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(result);
}

void Item12(Matrix*m)
{
	cout << "Укажите название файла: " << endl;
	string path;
	ClearBuffer();
	getline(cin, path);
	ClearBuffer();
	ifstream fin(path);
	bool write;//истина, если нужно (пере)записывать, ложь если не нужно
	if (fin.is_open())
	{
		fin.close();
		cout << "Перезаписать файл ? (y/N) ";
		string ans;
		do
		{
			getline(cin, ans);
		    ClearBuffer();
			if (ans == "yes" || ans == "YES" || ans == "Yes" || ans == "y" || ans == "Y")
			{
				write = true;
				break;
			}
			else
				if (ans == "no" || ans == "NO" || ans == "No" || ans == "n" || ans == "N")
				{
					write = false;
					break;
				}
				else
					cout << "Некорректный ввод, попробуйте еще раз" << endl;
		} while (true);//выход осуществится при корректном ввода ответа
	}
	else
		write = true;//если файла нет, то мы записываем 100%
	if (!write)//(пере)записывать не нужно
		return;
	ofstream fout(path);
	fout << m->rows << ' ' << m->colomns;
	for (int i = 0; i < m->rows; i++)
	{
		for (int j = 0; j < m->colomns; j++)
			fout << setw(4) << m->pA[i][j];
		fout << endl;
	}
	fout.close();
}

void ClearBuffer()
{
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
}

void Item13(Matrix*m)
{
	cout << "Укажите путь к файлу: " << endl;
	string path;
	ClearBuffer();
	getline(cin, path);
	ClearBuffer();
	ifstream fin(path);
	if (fin.is_open())
	{
		DeleteMatrix(m);
		fin >> m->rows >> m->colomns;
		CreateMatrix(m);
		for (int i = 0; i < m->rows; i++)
			for (int j = 0; j < m->colomns; j++)
				fin >> m->pA[i][j];
	}
	else
		cout << "Не удается открыть файл" << endl;
	fin.close();
}

void Item14(Matrix*m)
{
	cout << "Выберите порядок сортировки: " << endl << "s: змейкой" << endl << "e: улиткой" << endl << "a: муравьем" << endl;
	char ans;
	cin >> ans;
	ClearBuffer();
	Matrix*m_res = new Matrix(m->rows, m->colomns);//здесь будет храниться отсортированная матрица
	CreateMatrix(m_res);
	//записываем матрицу в линию для сортировки
	int n = m->rows*m->colomns;
	int*a = new int[n];
	for (int k = 0, i = 0; i < m->rows; i++)
		for (int j = 0; j < m->colomns; j++, k++)
			a[k] = m->pA[i][j];
	//сортируем методом слияний
	MergeSort(a, 0, n - 1);
	switch (ans)//заполняем матрицу отсортированной линией в зависимости от ввода
	{
	case 's': 
		for (int k = 0, column=0; k < n;)
		{
			for (int i=0; i < m_res->rows; k++, i++)
				m_res->pA[i][column] = a[k];
			column++;
			for (int i = m_res->rows - 1; k < n && i >= 0; k++, i--)
				m_res->pA[i][column] = a[k];
			column++;
		}
		break;
	case 'e':
		for (int k = 0, v = 0; k < n; v++)
		{
			for (int i = v; i < m_res->colomns - v; k++, i++)
				m_res->pA[v][i] = a[k];
			for (int i = v + 1; k < n && i < m_res->rows - v; k++, i++)
				m_res->pA[i][m_res->colomns - v - 1] = a[k];
			for (int i = m_res->colomns - v - 2; k < n && i >= v; i--, k++)
				m_res->pA[m_res->rows - v - 1][i] = a[k];
			for (int i = m_res->rows - v - 2; k<n && i>v; i--, k++)
				m_res->pA[i][v] = a[k];
		}
		break;
	case 'a':
		for (int k = 0; k < n; k++)
			m_res->pA[k / m_res->colomns][k%m_res->colomns] = a[k];
		break;
	default: cout << "Некорректный ввод" << endl; 
		return;
	}
	//выводим отсортированную матрицу
	for (int i = 0; i < m_res->rows; i++)
	{
		for (int j = 0; j < m_res->colomns; j++)
			cout << setw(4) << m_res->pA[i][j];
		cout << endl;
	}
	DeleteMatrix(m_res);
	delete[]a;
}

int temp[500];
void Merge(int*a, int start, int middle, int end)
{
	int i1 = start, i2 = middle + 1;
	int len = end - start + 1;
	for (int i = 0; i < len; i++)
		if (i2 > end || i1 <= middle&&a[i1] < a[i2])
			temp[i] = a[i1++];
		else
			temp[i] = a[i2++];
	for (int i = 0; i < len; i++)
		a[i + start] = temp[i];
}

void MergeSort(int*a, int L, int R)
{
	if (L == R)
		return;
	int M = (L + R) / 2;
	MergeSort(a, L, M);
	MergeSort(a, M + 1, R);
	Merge(a, L, M, R);
}